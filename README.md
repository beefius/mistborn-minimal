# Mistborn-Minimal 

# Quickstart

```
cd /opt
clone.sh
* (supply config/wg.conf ?)
build.sh
```

Forwarding Traffic over second connection:

  * Wireguard - requires wg.conf file in config/ as wg.conf
  * OVPN - requires ovpn.conf file in config/ as ovpn.conf

Get default admin Wireguard profile
*wait 1 minute after "Mistborn Installed" message*
```
sudo mistborn-cli getconf 
```

Connect via Wireguard then visit `http://home.mistborn`


# TO DO
  * Enable ENV var to determine if forwarding-wg-proxy yes/no
  * change build.sh and process, use Docker Swarm, set *.conf files as secret_files


# Network Diagram
![Mistborn Network Diagram](https://gitlab.com/cyber5k/public/-/raw/master/graphics/mistborn_network.png)

Mistborn protects your data in a variety of ways:
- All of your devices are protected wherever they go with the Wireguard VPN protocol
- The Mistborn firewall blocks unsolicited incoming internet packets
- Pi-hole running on Mistborn blocks outgoing internet requests to configurable blocked domains (ads, malicious/phishing domains, etc.) 

See the [Mistborn Network Security](https://gitlab.com/cyber5k/mistborn/-/wikis/Mistborn-Network-Security) wiki page to see more network diagrams and the network scan results for Mistborn.

# Gateways
We were getting frustrated at being forced to choose between being connected to our VPN and using streaming services that we have paid for.

![Netflix blocked](https://gitlab.com/cyber5k/public/-/raw/master/graphics/netflix_blocked.png)

*Netflix blocking my connections that it sees coming from a DigitalOcean droplet*

In Mistborn, Gateways are upstream from the VPN server so connections to third-party services (e.g. Netflix, Hulu, etc.) will appear to be coming from the public IP address of the Gateway. I setup a Gateway at home (Raspberry Pi with `wireguard` and `openresolv` installed) and with our Mistborn on DigitalOcean, all Wireguard profiles created with this Gateway will appear to be coming from my house and are not blocked. No port-forwarding required (assuming Mistborn is publicly accessible).

![Mistborn Gateway Diagram](https://gitlab.com/cyber5k/public/-/raw/master/graphics/gateway_network.png)

The Gateway adds an extra network hop. DNS is still resolved in Mistborn so pihole is still blocking ads.


# Installation
Mistborn is regularly tested on Ubuntu 20.04 LTS (DigitalOcean droplet with 2 GB RAM). It has also been successfully used on Debian Buster and Raspbian Buster systems (though not regularly tested). Make sure to install OS updates and restart before installing Mistborn (Wireguard installs differently on recent kernels).

Clone the git repository and run the install script:
```
git clone https://gitlab.com/cyber5k/mistborn.git
sudo -E bash ./mistborn/scripts/install.sh
```

Running `install.sh` will do the following:
- create a `mistborn` system user
- clone the mistborn repo to `/opt/mistborn`
- setup iptables and ip6tables rules and chains
- install iptables-persistent
- install Docker
- install OpenSSH
- install Wireguard
- install Cockpit (optional)
- create a `cockpit` system user (if Cockpit is installed)
- configure unattended-upgrades
- generate a self-signed TLS certificate/key (WebRTC functionality requires TLS)
- create and populate traefik.toml
- create `/opt/mistborn_volumes` and setup folders for services that will be mounted within
- backup original contents of `/opt/mistborn_volumes` in `/opt/mistborn_backup`
- Pull docker images for base.yml
- Build docker images for base.yml
- Disable competing DNS services (systemd-resolved and dnsmasq)
- copy Mistborn systemd service files to `/etc/systemd/system`
- start and enable Mistborn-base

# Non-Interactive Installation
In order to install without interaction some environment variables need to be pre-set. 

## Environment Variables
See the environment variables needed in `./scripts/noninteractive/.install_barebones`

## Example Noninteractive Install
This will perform a noninteractive install with the default environment variables set in `.install_barebones`.
```
git clone https://gitlab.com/cyber5k/mistborn.git
sudo -E bash -c "source ./mistborn/scripts/noninteractive/.install_barebones && ./mistborn/scripts/install.sh"
```

# Post-Installation
When Mistborn-base starts up it will create volumes, initialize the PostgreSQL database, start pihole, run Django migrations and then check to see if a Mistborn superuser named `admin` exists yet. If not, it will create the superuser `admin` along with an accompanying default Wireguard configuration file and start the Wireguard service. You can watch all of this happen with:
```
sudo journalctl -xfu Mistborn-base
```

The default Wireguard configuration file for `admin` may be obtained via:
```
sudo mistborn-cli getconf 
```
Please notice that the following lines are **NOT** part of the Wireguard config:
```
Starting mistborn_production_postgres ... done
Starting mistborn_production_redis    ... done
PostgreSQL is available
```

The Wireguard config will look like this:
```
# "10.15.91.2" - WireGuard Client Profile
[Interface]
Address = 10.15.91.2/32
# The use of DNS below effectively expands to:
#   PostUp = echo nameserver 10.15.91.1 | resolvconf -a tun.%i -m 0 -x
#   PostDown = resolvconf -d tun.%i
# If the use of resolvconf is not desirable, simply remove the DNS line
# and use a variant of the PostUp/PostDown lines above.
# The IP address of the DNS server that is available via the encrypted
# WireGuard interface is 10.15.91.1.
DNS = 10.15.91.1
PrivateKey = cPPflVGsxVFw2/lMmhiFTXMmH345bGqoqArD/NgjiXU=

[Peer]
PublicKey = DfIV1urYZXqXKiU4rOSfO0Iu589pEO+59dHV5w5N0mU=
PresharedKey = Z1SO5NuAnZ7JhzVCuUnYOQLWOQYmMoqG0pG1SNXUlh0=
AllowedIPs = 0.0.0.0/0,::/0
Endpoint = <Mistborn public IP address>:39207
```

## Login via Wireguard
[Install wireguard](https://www.wireguard.com/install/) on your computer. If you get a `resolvconf: command not found` error when starting Wireguard then install openresolv: `sudo apt-get install -y openresolv`
- Copy the text of the default admin Wireguard config to `/etc/wireguard/wg_admin.conf` on your computer
- Run `sudo systemctl start wg-quick@wg_admin`
- Run `sudo systemctl enable wg-quick@wg_admin`
- Open your browser and go to "http://home.mistborn"
- Browse your Mistborn system!
**Note:** The home.mistborn server takes a minute to come up after Mistborn is up (collectstatic on all that frontend JavaScript and CSS)



# Gateway Setup
Mistborn will generate the Wireguard configuration script for the Gateway. From a base Ubuntu/Debian/Raspbian operating system the following packages are recommended to be installed beforehand:

## Gateway Requirements
- Wireguard (you can consult the Mistborn Wireguard installer: `mistborn/scripts/subinstallers/wireguard.sh`)
- Openresolv (a Wireguard dependency that is also installed via the Mistborn Wireguard installer)
- Fail2ban

## Install Gateway Wireguard config file
On Mistborn:
- Click `View Config` on the Gateways tab in Mistborn
- Highlight the config
- Copy (Ctrl-C)

On Gateway:
- Paste the config to `/etc/wireguard/gateway.conf`
- Run `sudo systemctl start wg-quick@gateway`
- Run `sudo systemctl enable wg-quick@gateway`

   |

## TLS Certificate
Some apps require TLS (HTTPS). All traffic to Mistborn domains already occurs over Wireguard but to keep apps running, a TLS certificate exists for Mistborn and can be imported into your device's trusted credentials in the security settings. This certificate is checked every day and will be re-generated when expiration is less than 30 days away.

The TLS certificate can be found here:
```
/opt/mistborn_volumes/base/tls/cert.crt
```

## How do I SSH into Mistborn
If Mistborn is installed via SSH then an iptables rule is added allowing external SSH connections from the same source IP address only. If Mistborn was installed locally then no external SSH is permitted.

SSH is permitted from any device connected to Mistborn by Wireguard.

Password authentication in enabled. Fail2ban blocks IPs with excessive failed login attempts.

You can SSH using the Mistborn domain when connected by Wireguard:
```
ssh user@home.mistborn
```

## How do I change the upstream DNSCrypt servers?
The upstream servers used by dnscrypt-proxy are set in:  

`base.yml`:
```
services:
...
  dnscrypt-proxy:
  ...
    environment:
    ...
      - DNSCRYPT_SERVER_NAMES=[...]
```

The available options are here: https://download.dnscrypt.info/dnscrypt-resolvers/v2/public-resolvers.md

# Troubleshooting

Once you're connected to Wireguard you should see .mistborn domains and the internet should work as expected. Be sure to use http (http://home.mistborn). Wireguard is the encrypted channel so there's usually no need to bother with TLS certs (WebRTC functionality and some mobile apps require TLS so it is available). Here are some things to check if you have issues:

Check if you can ping an external IP address:
```
ping 1.1.1.1
```

Check if you can resolve local DNS queries:
```
dig home.mistborn
```

Check if you can resolve external DNS queries:
```
dig cyber5k.com
```

See if any docker containers are stopped:
```
sudo docker container ls -a
```

Check the running log for Mistborn-base:
```
sudo journalctl -xfu Mistborn-base
```

Mistborn-base is a systemd process and at any time restarting it should get you to a working state:
```
sudo systemctl restart Mistborn-base
```

The Wireguard processes run independently of Mistborn and will still be up if Mistborn is down. You can check running Wireguard interfaces with:
```
sudo wg show
```
Note the Mistborn naming convention for Wireguard interfaces on the server is wg<listening port>. So if the particular Wireguard process is listening on UDP port 56392 then the interface will be named wg56392 and the config will be in `/etc/wireguard/wg56392.conf`

The `dev/` folder contains a script for completing a hard reset: destroying and rebuilding the system from the original backup:
```
sudo ./dev/rebuild.sh
```


## Attack Surface

See the [Mistborn Network Security](https://gitlab.com/cyber5k/mistborn/-/wikis/Mistborn-Network-Security) wiki entry.

- **Wireguard**: Wireguard is the only way in to Mistborn. When new Wireguard profiles are generated they are attached to a random UDP port. Wireguard does not respond to unauthenticated traffic. External probes on the active Wireguard listening ports are not logged and do not appear on the Metrics page.
- **SSH**: If Mistborn is installed over SSH (most common) then an iptables rule is added allowing future SSH connections from the same source IP address. All other external SSH is blocked. Internal SSH (over the Wireguard tunnels) is allowed. Password authentication is allowed. The SSH key for the `mistborn` user is only accepted from internal source IP addresses. Fail2ban is also installed.
- **Traefik**: Iptables closes web ports (TCP 80 and 443) from external access and additonally all web interfaces are behind the Traefik reverse-proxy. All web requests (e.g. home.mistborn) must be resolved by Mistborn DNS (Pihole/dnsmasq) and originate from a Wireguard tunnel.
- **Docker**: When Docker exposes a port it creates a PREROUTING rule in the NAT table to catch eligible network requests. This means that even if your INPUT chain policy is DROP, your docker containers with exposed ports can receive and respond to traffic. Whenever Mistborn brings up a docker container with an exposed port it creates an iptables rule to block external traffic to that service. 

## Firewall
- **IPtables**: Iptables rules and chains are manipulated directly. If UFW is present it is disabled. IPtables-persistent is used to save a simple set of secure default rules (most importantly setting the INPUT and FORWARD policies to DROP and allowing ESTABLISHED and RELATED traffic) that will be effective immediately upon system startup. Additional rules and chains are created by Docker on startup. Mistborn also creates some iptables chains during installation that are saved in the persistent rules. Mistborn iptables chains and rules are designed to work with Docker's with logic that is easy to follow. A power cycle will always result in a working state.
- **PostUp/PostDown**: Wireguard configuration files on Mistborn include PostUp and PostDown directives that set routes and iptables rules for each Wireguard client individually.
- **Wireguard**: There is a one-to-one mapping between each Wireguard client and server instance listening on Mistborn. By default Wireguard clients cannot talk directly to each other but can use shared services and resources on Mistborn (e.g. Syncthing, Nextcloud, Jitisi, etc). Toggling the "client-to-client" option will enable direct client-to-client communication.
- **Metrics**: In addition to the iptables INPUT policy set to DROP, an iptables chain exists that logs the packet meta data before dropping it. Mistborn redirects packets that will be dropped to this chain instead. A summary of the data about these dropped packets (unsolicited network traffic) can be found on the Metrics page.
- **Coppercloud**: Coppercloud works by populating ipsets with the ipset module in iptables to DROP (blacklist) or ACCEPT (whitelist) a given set of IP addresses. Upon system startup a celery task will compile the IP addresses, create the ipsets, and iptables rules.

## Additonal Notes
- Interface names are not hardcoded anywhere in Mistborn. Two commands that are used in different circumstances to determine the default network interface and the interface that would route a public IP address are: `ip -o -4 route show to default` and `ip -o -4 route get 1.1.1.1`.
- The "Update" button will pull updated Docker images for mistborn, postgresql, redis, pihole, and dnscrypt. Those services will then be restarted.
- The generated TLS certificate has an RSA modulus of 4096 bits, is signed with SHA-256, and is good for 397 days. The certificate is checked daily and will regenerate when expiration is within 30 days.
- Outbound UDP on port 53 is blocked. All DNS requests should be handled by the dnscrypt_proxy service and if any client, service, etc. tries to circumvent that it is blocked.
- Unattended upgrades are set to automatically install operating system security updates.

# Roadmap (not necessarily in order)
Many features and refinements are in the works at various stages including:

- Plugins for Extra Services (enabling third-party development)
- Plugin repository
- IPv6 support
- Integration with RaspAP to enable managing an Access Point for local network connections
- Internal network scan tool and feedback
- Anomaly detection in network traffic
